package Algorithm;
/**

 * This program creates an undirected graph G from an input file
 * where the edges are represented as pair of integers
 * and integers represent vertices of the graph.
 * It is assumed that there is no duplicate edge and self loop in the graph.
 * */
import java.io.*;
import java.lang.reflect.Array;
import java.util.*;

import utils.SetOperations;
import utils.Vscore;

public class Graph {
	public ArrayList <HashSet<Integer>> AdjList;
	public ArrayList <HashSet<Integer>> M ; // used in Peamc
	
	private Vscore[] S;
	private int degeneracy; // contains degeneracy value
	long [] tri ;
	int [] degen ;
	
	HashMap <String, Integer> vertexIndex ;
	int numberOfVertices;
	public ArrayList <Integer> vertices;

	private Map<Integer, Integer> degenscores; // (key, value) pair; key = vertex, value = degeneracy score
	private Map<Integer, Integer> trianglecount; // (key, value) pair; key = vertex, value = triangle count
	ArrayList<Integer> degreeOrder;
	ArrayList<Integer> triangleOrder;
	ArrayList<Integer> degenOrder;
	private static long ttime;
	
	public Graph() {
		this.AdjList = new ArrayList<>();
		this.vertexIndex = new HashMap<>() ;
		this.numberOfVertices = 0 ;
		this.vertices = new ArrayList<>() ;
		degreeOrder = new ArrayList<>();
		triangleOrder = new ArrayList<>();
		degenOrder = new ArrayList<>();
		trianglecount = new HashMap<>();
	}

	public Graph(Graph G) {
		this.AdjList = new ArrayList<>(G.AdjList);
		this.vertexIndex = new HashMap<>() ;
		this.numberOfVertices = 0 ;
		this.vertices = new ArrayList<>() ;
		degreeOrder = new ArrayList<>();
		triangleOrder = new ArrayList<>();
		degenOrder = new ArrayList<>();
		trianglecount = new HashMap<>();
	}

	public void buildM () {
		M = new ArrayList<>() ;
		for (Integer vertex : this.vertices) {
			M.add(new HashSet<>()) ;
			for (Integer neighbor : AdjList.get(vertex)) {
				if (neighbor > vertex)
					M.get(vertex).add(neighbor) ;
			}
		}
	}

	public boolean isAllDigits (String s) {
		for (int i = 0; i < s.length(); i++) {
			if (Character.isDigit(s.charAt(i)) == false) return false ;
		}
		return true ;
	}

	public int addVertex (String vertex) {
		if (!vertexIndex.containsKey(vertex)) {
			this.vertices.add(numberOfVertices) ;
			degreeOrder.add(numberOfVertices);
			degenOrder.add(numberOfVertices);
			triangleOrder.add(numberOfVertices);
			this.vertexIndex.put(vertex, numberOfVertices++) ;
			this.AdjList.add(new HashSet<>()) ;
		}
		return vertexIndex.get(vertex) ;
	}

	public Graph(String args) {
		this.AdjList = new ArrayList<>();
		this.vertexIndex = new HashMap<>() ;
		this.numberOfVertices = 0 ;
		this.vertices = new ArrayList<>() ;
		this.ttime = 0;
		degreeOrder = new ArrayList<>();
		triangleOrder = new ArrayList<>();
		degenOrder = new ArrayList<>();
		trianglecount = new HashMap<>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(args));
			String line;
			while ((line = br.readLine()) != null) {
				String[] splits = line.split("\\s+");
				if (splits.length < 2) continue ;
				if (isAllDigits(splits [0]) == false || isAllDigits(splits [1]) == false) continue ;

				addEdge(splits[0],splits[1]);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Triangle Computation Takes Time: " + ttime/1000 + " sec.");
		
		Collections.sort(degreeOrder, (a,b) -> this.degreeOf(a) - this.degreeOf(b));
		Collections.sort(triangleOrder, (a,b) -> this.tcountAt(a) - this.tcountAt(b));
	}

	public void addEdge(String x, String y) {
		
		int u = addVertex(x);
		int v = addVertex(y);
		
		if (u == v) return ;
		if (this.AdjList.get(v).contains(u) == false) {
			long t1 = System.currentTimeMillis();
			
			if (!trianglecount.keySet().contains(u))
				trianglecount.put(u, 0);
			if (!trianglecount.keySet().contains(v))
				trianglecount.put(v, 0);

			ttime += (System.currentTimeMillis()-t1);

			this.AdjList.get(v).add(u) ;
			this.AdjList.get(u).add(v) ;


			t1 = System.currentTimeMillis();

			// update the triangles count
			HashSet<Integer> commonNeighborhood = SetOperations.intersect(AdjList.get(u), AdjList.get(v));

			for (int w : commonNeighborhood) {

				int tcount_u = 0;
				int tcount_v = 0;
				int tcount_w = 0;

				tcount_u = trianglecount.get(u);
				tcount_v = trianglecount.get(v);
				tcount_w = trianglecount.get(w);

				tcount_u++;
				tcount_v++;
				tcount_w++;

				trianglecount.put(u, tcount_u);
				trianglecount.put(v, tcount_v);
				trianglecount.put(w, tcount_w);
			}

			ttime += (System.currentTimeMillis()-t1);
		}
	}
	
	public boolean containsEdge(int u, int v){
	    if(AdjList.get(u) != null){
	        if(AdjList.get(u).contains(v))
	            return true;
	        else
	            return false;
	    }
	    return false;
	}

	public void removeEdge(int u, int v) {
		if (AdjList.get(u) != null) {
			if (AdjList.get(u).contains(v)) {
				AdjList.get(u).remove(v);
				AdjList.get(v).remove(u);
			}
		}

	}

	public void print() {
		for (int v = 0 ; v < numberOfVertices ; v ++){
			System.out.print(v + ":");
			for (int u : AdjList.get(v)) {
				System.out.print(u + ";");
			}
			System.out.println();
		}
		System.out.println();
	}

	public void print(FileWriter fw) {
		try {
			for (int v = 0 ; v < numberOfVertices ; v ++){
				fw.write(v + " ");
				for (int u : AdjList.get(v)) {
					fw.write(u + " ");
				}
				fw.write("\n");
			}
		} catch (IOException e) {
		}
	}

	public int MaxDegree() {
		int max = Integer.MIN_VALUE;
		for (int v = 0 ; v < numberOfVertices ; v ++){
			int size = AdjList.get(v).size();
			if (max < size) {
				max = size;
			}
		}
		return max;
	}

	public int MinDegree() {
		int min = Integer.MAX_VALUE;
		for (int v = 0 ; v < numberOfVertices ; v ++){
			int size = AdjList.get(v).size();
			if (min > size) {
				min = size;
			}
		}
		return min;
	}

	public int AvgDegree() {
		int sum = 0;
		for (int v = 0 ; v < numberOfVertices ; v ++){
			sum += AdjList.get(v).size();
		}
		return (sum / (2 * AdjList.size()));
	}

	public int numE() {
		int size = 0;
		for (int v = 0 ; v < numberOfVertices ; v ++){
			size += AdjList.get(v).size();
		}
		return size / 2;
	}

	public int numV() {
		return this.numberOfVertices ;
	}

	public int getSize() {
		int size = 0;
		for (int v = 0 ; v < numberOfVertices ; v ++){
			size++;
			size += AdjList.get(v).size();
		}
		return size;
	}

	public ArrayList<Integer> getDegenOrdering() {
		return degenOrder;
	}
	
	public ArrayList<Integer> getDegreeOrdering(){
		return degreeOrder;
	}
	
	public ArrayList<Integer> getTriangleOrdering(){
		return triangleOrder;
	}

	public int degreeOf(int vertex) {
		return this.AdjList.get(vertex).size();
	}

	public int degeneracyOf(int vertex) {
		return degenscores.get(vertex);
	}

	public int tcountAt(int vertex) {
		return trianglecount.get(vertex);
	}

	public Collection<Integer> neighborsOf(int u) {
		return this.AdjList.get(u);
	}

	public void clear() {
		AdjList.clear();
	}

	// Adopted this code from
	// https://github.com/jgrapht/jgrapht/blob/master/jgrapht-core/src/main/java/org/jgrapht/alg/scoring/Coreness.java
	public void computeDegeneracy() {
		if (degenscores != null) {
			return;
		}

		degenscores = new HashMap<>();

		S = new Vscore[AdjList.size()];

		int vcount = 0;

		degeneracy = 0;

		/* initialize buckets */
		int n = AdjList.size();
		int maxDegree = n - 1;
		Set<Integer>[] buckets = (Set<Integer>[]) Array.newInstance(Set.class, maxDegree + 1);

		for (int i = 0; i < buckets.length; i++) {
			buckets[i] = new HashSet<>();
		}

		int minDegree = n;
		Map<Integer, Integer> degrees = new HashMap<>();

		for (int v = 0 ; v < numberOfVertices ; v ++){
			int d = degreeOf(v);
			buckets[d].add(v);
			degrees.put(v, d);
			minDegree = Math.min(minDegree, d);
		}

		/* Extract from bucket */
		while (minDegree < n) {
			Set<Integer> b = buckets[minDegree];
			if (b.isEmpty()) {
				minDegree++;
				continue;
			}

			int v = b.iterator().next();
			b.remove(v);

			degenscores.put(v, minDegree);
			S[vcount] = new Vscore(v, minDegree);
			vcount++;

			degeneracy = Math.max(degeneracy, minDegree);

			for (int u : AdjList.get(v)) {
				int uDegree = degrees.get(u);
				if (uDegree > minDegree && !degenscores.containsKey(u)) {
					buckets[uDegree].remove(u);
					uDegree--;
					degrees.put(u, uDegree);
					buckets[uDegree].add(u);
					minDegree = Math.min(minDegree, uDegree);
				}
			}
		}
		//Arrays.parallelSort(S);
		Collections.sort(degenOrder, (a,b) -> this.degeneracyOf(a) - this.degeneracyOf(b));
	}

	public void triangleCount() {
		tri = new long [this.numV()] ;
		for (int i = 0 ; i < this.numV() ; i ++) {
			triangleCount(i);
		}
	}

	public void triangleCount (int v) {
		for (int neighbor : this.AdjList.get(v)) {
			for (int neighbor2 : this.AdjList.get(v)) {
				if (this.AdjList.get(neighbor).contains(neighbor2))
					tri [v] ++ ;
			}
		}
	}

	public void degen () {
		degen = new int [this.numV()] ;
		TreeSet <Long> set = new TreeSet <Long>() ;
		long shift = (long)1e9 ;
		for (int i = 0; i < this.numV(); i++) {
			degen [i] = this.AdjList.get(i).size();
			set.add(degen [i] * shift + i) ;
		}
		while (!set.isEmpty()) {
			long fst = set.iterator().next() ;
			set.remove(fst) ;
			int id = (int)(fst % shift) ;
			for (int neighbor : this.AdjList.get(id)) {
				if (degen [neighbor] > degen [id]) {
					set.remove(degen [neighbor] * shift + neighbor);
					degen [neighbor] -- ;
					set.add(degen [neighbor] * shift + neighbor) ;
				}
			}
		}
	}



	public void printedges(FileWriter fw) throws Exception {
		String[] edges = new String[numE()];

		int count_edges = 0;
		for (int v = 0 ; v < numberOfVertices ; v ++){
			for (int u : AdjList.get(v)) {
				if (u < v) {
					edges[count_edges] = u + " " + v;
					count_edges++;
				}
			}
		}

		System.out.println(count_edges);

		// randomly shuffling the edges
		for (int i = 0; i < edges.length; i++) {
			int r = i + (int) (Math.random() * (edges.length - i));
			String temp = edges[r];
			edges[r] = edges[i];
			edges[i] = temp;
		}

		for (int i = 0; i < edges.length; i++) {
			fw.write(edges[i] + "\n");
		}
		fw.close();

	}

	public void printVertices(FileWriter fw) throws Exception {
		for (int v = 0 ; v < numberOfVertices ; v ++){
			fw.write(v + "\n");
		}

	}

	public int getId(int v) {
		return this.vertexIndex.get(String.valueOf(v));
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", args[1]);

		System.out.println("ParMCEDegree : degree based vertex ordering");
		System.out.println("Input Graph: " + args[0]);
		System.out.println("Number of threads used: " + args[1]);

		Graph G = new Graph(args[0]);

		long t1 = System.currentTimeMillis();
		G.degen();
		System.out.println("Time taken for degenarcy:" + (System.currentTimeMillis() - t1) / 1000.) ;

		t1 = System.currentTimeMillis() ;
		G.triangleCount();
		System.out.println("Time taken for trianle count:" + (System.currentTimeMillis() - t1) / 1000.) ;

	}
}
