package Algorithm.MCE;
/**This Algorithm generates all maximal cliques of an undirected graph G
 * based on Tomita et al. with title "The worst-case time complexity for generating all maximal cliques and computational experiments"
 * */

import java.util.*;

import Algorithm.Graph;
import utils.MurmurHash3;
import utils.SetOperations;

import java.io.*;

public class TTT {

	private Set<Integer> CAND;
	private Set<Integer> FINI;
	private TreeSet<Integer> K;
	public HashSet<BitSet> CLQ;
	public int clqcnt;
	private FileWriter cw;

	private static Graph G;
	
	static long loopoverhead;

	static long count;
	
	private static HashMap<Integer, Integer> cliqueSizes;

	private static HashMap<Integer, HashSet<Integer>> MapVE;

	public TTT(Graph g, String ofname) throws IOException {

		this.G = g;

		count = 0;
		
		loopoverhead = 0;
		
		cliqueSizes = new HashMap<>();

		CAND = new HashSet<Integer>();
		FINI = new HashSet<Integer>();
		K = new TreeSet<Integer>();
		CLQ = new HashSet<BitSet>();
		clqcnt = 1;
		for (int v : G.vertices) {
			CAND.add(v);
		}
		expand(K, CAND, FINI);
	}


	public void expand(TreeSet<Integer> K, Set<Integer> CAND, Set<Integer> FINI)
			throws IOException {
		if (CAND.isEmpty() && FINI.isEmpty()) {
			// System.out.println(count + ":" + K);
			count++;
			
			int size_of_k = K.size();
			
			if(cliqueSizes.get(size_of_k) == null) {
				cliqueSizes.put(size_of_k, 1);
			} else {
				int s = cliqueSizes.get(size_of_k);
				cliqueSizes.put(size_of_k, s+1);
			}
			
			return;
		}
		if(CAND.isEmpty() && !FINI.isEmpty())
			return;
		int u = find_u(CAND, FINI);

		Collection<Integer> NghOfu = Ngh(G, u);

		Iterator<Integer> candit = CAND.iterator();

		while (candit.hasNext()) {

			int q = candit.next();
			if (!NghOfu.contains(q)) {

				K.add(q);
				
				//long start = System.currentTimeMillis();

				Collection<Integer> NghOfq = Ngh(G, q);

				HashSet<Integer> CANDq = SetOperations.intersect(CAND, NghOfq);

				HashSet<Integer> FINIq = SetOperations.intersect(FINI, NghOfq);
				
				//loopoverhead += (System.currentTimeMillis() - start);

				expand(K, CANDq, FINIq);

				candit.remove(); // CAND - {q}
				
				K.remove(q);

				FINI.add(q); // FINI union {q}
			}
		}
	}

	/*
	 * public Set<Integer> Ngh(int u) { return G.AdjList.get(u); }
	 */

	public Collection<Integer> Ngh(Graph G, int u) {
		return G.AdjList.get(u);
	}

	public int find_u(Set<Integer> CAND, Set<Integer> FINI /*
															 * Set<String> SUBG
															 */) {

		int size = -1;
		int v = 0;

		for (int u : CAND) {
			HashSet<Integer> Q = SetOperations.intersect(Ngh(G,u), CAND);
			int tmp = Q.size();
			if (size <= tmp) {
				size = tmp;
				v = u;
			}
		}

		for (int u : FINI) {
			HashSet<Integer> Q = SetOperations.intersect(Ngh(G,u), CAND);
			int tmp = Q.size();
			if (size <= tmp) {
				size = tmp;
				v = u;
			}
		}
		return v;

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	

		System.out.println("Sequential TTT Algorithm");
		System.out.println("Input Graph: " + args[0]);
		
			
		Graph G = new Graph(args[0]);
		
		System.out.println("Graph Reading Complete");
		
		System.out.println("Number of Vertices: " + G.numV());
		System.out.println("Number of Edges: " + G.numE());

		try {
			long t1 = System.currentTimeMillis();
			new TTT(G, "output_TTT");
			long elapsed = System.currentTimeMillis() - t1;
			System.out.println("number of maximal cliques: " + count);
			System.out.println("Sequential time taken to compute maximal cliques in" + args[0] + ": " + elapsed/1000 + " sec.");
			System.out.println("Total loop overhead : " + loopoverhead/1000. + " sec.");
			
			System.out.println("max xclique size frequency distribution");
			
			System.out.println("size,Frequency");
			
			int max_size = Integer.MIN_VALUE;
			int min_size = Integer.MAX_VALUE;
			int total_size = 0;
			int count = 0;
			for(int s : cliqueSizes.keySet()) {
				int freq = cliqueSizes.get(s);
				System.out.println(s + "," + freq);
				if(s < min_size) {
					min_size = s;
				}
				if(s > max_size) {
					max_size = s;
				}
				
				total_size += freq*s;
				count += freq;
			}
			
			System.out.println("size of maximum clique: " + max_size);
			System.out.println("minimum size of a maximal clique: " + min_size);
			System.out.println("Average size of a maximal clique: " + total_size/count);
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// new Tomita(G);
	}

}
