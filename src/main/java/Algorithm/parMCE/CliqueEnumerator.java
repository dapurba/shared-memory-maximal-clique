package Algorithm.parMCE;

/**
 * The implementation is based on the paper "Genome-Scale Computational Approaches to Memory-Intensive Applications
 * in System Biology"
 * @author adas
 *
 */

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import Algorithm.Graph;

public class CliqueEnumerator {

	private static Graph G;

	private Set<kCliqueSublist>[] L;

	private final int N; // number of vertices in the graph

	// private int[] vArray;
	// private final Map<Integer, Integer> vToIndex;
	private Map<Integer, BitSet> vToNghMap;

	private static AtomicLong maxCliqueCount;

	private int K;
	
	static AtomicInteger I;

	public CliqueEnumerator(String graph, String numThreads) {

		System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", numThreads);
		G = new Graph(graph);

		N = G.AdjList.size();

		// vArray = new int[G.AdjList.size()];
		// vToIndex = new HashMap<>();
		vToNghMap = new HashMap<>();

		maxCliqueCount = new AtomicLong(0);

		K = 2;

		int index = 0;

		// for(int v : G.vertices) {
		// vArray[index] = v;
		// vToIndex.put(v, index);
		// index++;
		// }

		// construct vertex to neighbors bitmap
		for (int v : G.vertices) {
			BitSet b = new BitSet(N);
			for (int u : G.neighborsOf(v)) {
				b.set(u);
			}
			vToNghMap.put(v, b);
		}

		L = new Set[2];
		
		L[0] = ConcurrentHashMap.newKeySet();
		L[1] = ConcurrentHashMap.newKeySet();

		for (int v : G.vertices) {

			kCliqueSublist l = new kCliqueSublist(v, G.neighborsOf(v), N, vToNghMap);

			if (l.getListSize() == 1) {
				if (l.isMaxClique(vToNghMap)) {
					maxCliqueCount.incrementAndGet();
				}
			} else {
				L[0].add(l);
			}
		}
		
		I = new AtomicInteger(0);

		//System.out.println(maxCliqueCount);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		System.out.println("Algorithm CliqueEnumerator");
		System.out.println("Input Graph: " + args[0]);
		System.out.println("Number of Threads used: " + args[1]);
		
		long t1 = System.currentTimeMillis();

		CliqueEnumerator enumerator = new CliqueEnumerator(args[0], args[1]);

		enumerator.enumerate();
		
		long elapsed = System.currentTimeMillis() - t1;
		
		
		
		System.out.println("Total time elapsed: " + elapsed/1000 + " sec.");
		
		System.out.println("Number of maximal cliques: " + maxCliqueCount.get());

	}

	private void enumerate() {

		

		while (!L[I.get()].isEmpty()) {
			// special handling of maximal clique of size 2
			
			L[I.get()].parallelStream().forEach(l -> {
				TreeSet<Integer> C = l.getClique();
				ArrayList<Integer> vlist = l.getVList();
				BitSet cnSk = l.getCommonNeighbors();
				for (int i = 0; i < vlist.size(); i++) {

					int v = vlist.get(i);

					BitSet cnSkPlusOne = (BitSet) cnSk.clone();

					cnSkPlusOne.and(vToNghMap.get(v));

					if ((K == 2) && cnSkPlusOne.isEmpty()) {
						maxCliqueCount.incrementAndGet();
						continue;
					}

					ArrayList<Integer> newList = new ArrayList<>();

					for (int j = i + 1; j < vlist.size(); j++) {

						int w = vlist.get(j);

						if (G.containsEdge(v, w)) {

							BitSet tempCnSkPlusOne = (BitSet) cnSkPlusOne.clone();

							tempCnSkPlusOne.and(vToNghMap.get(w));

							if (!tempCnSkPlusOne.isEmpty()) {
								newList.add(w);
							} else {
								maxCliqueCount.incrementAndGet();
							}
						}
					}

					if (newList.size() > 1) {

						TreeSet<Integer> C1 = new TreeSet<>();
						C1.addAll(C);
						C1.add(v);
						kCliqueSublist l1 = new kCliqueSublist(C1, newList, cnSkPlusOne);
						L[1 - I.get()].add(l1);
					}

				}
			});
			/*for (kCliqueSublist l : L[I]) {
				TreeSet<Integer> C = l.getClique();
				ArrayList<Integer> vlist = l.getVList();
				BitSet cnSk = l.getCommonNeighbors();
				for (int i = 0; i < vlist.size(); i++) {

					int v = vlist.get(i);

					BitSet cnSkPlusOne = (BitSet) cnSk.clone();

					cnSkPlusOne.and(vToNghMap.get(v));

					if ((K == 2) && cnSkPlusOne.isEmpty()) {
						maxCliqueCount++;
						continue;
					}

					ArrayList<Integer> newList = new ArrayList<>();

					for (int j = i + 1; j < vlist.size(); j++) {

						int w = vlist.get(j);

						if (G.containsEdge(v, w)) {

							BitSet tempCnSkPlusOne = (BitSet) cnSkPlusOne.clone();

							tempCnSkPlusOne.and(vToNghMap.get(w));

							if (!tempCnSkPlusOne.isEmpty()) {
								newList.add(w);
							} else {
								maxCliqueCount++;
							}
						}
					}

					if (newList.size() > 1) {

						TreeSet<Integer> C1 = new TreeSet<>();
						C1.addAll(C);
						C1.add(v);
						kCliqueSublist l1 = new kCliqueSublist(C1, newList, cnSkPlusOne);
						L[1 - I].add(l1);
					}

				}

			}*/

			L[I.get()].clear();
			I = new AtomicInteger(1 - I.get());
			K = K + 1;
		}

	}

}
