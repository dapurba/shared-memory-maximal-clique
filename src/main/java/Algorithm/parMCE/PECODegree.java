package Algorithm.parMCE;


/***
 * PECO-REDUCE
 */

import java.util.*;

import Algorithm.Graph;
import utils.MurmurHash3;
import utils.SetOperations;

import java.io.*;
import java.util.concurrent.atomic.*;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ForkJoinPool;

public class PECODegree {


	private static Collection<BitSet> CLQ;

	private static ConcurrentMap<Long, Integer> timeToFrequency;
	public int clqcnt;
	private FileWriter cw;

	private static Graph G;

	static AtomicLong count;

	private static HashMap<Integer, HashSet<Integer>> MapVE;

	public PECODegree(Graph g, String ofname) throws IOException {

		this.G = g;

		count = new AtomicLong(0);

		CLQ = ConcurrentHashMap.newKeySet();

		timeToFrequency = new ConcurrentHashMap<>();
		
		G.vertices.parallelStream().forEach(v -> {
			Set<Integer> CAND = new HashSet<Integer>();
			Set<Integer> FINI = new HashSet<Integer>();
			TreeSet<Integer> K = new TreeSet<Integer>();
			
			
			HashMap<Integer, HashSet<Integer>> adjacency = new HashMap<>();
			
			adjacency = createGraph(G.AdjList.get(v));
			
			K.add(v);
			
			for(int w : G.AdjList.get(v)){
				if (G.degreeOf(w) > G.degreeOf(v))
					CAND.add(w);
				else if(G.degreeOf(w) < G.degreeOf(v))
					FINI.add(w);
				else {
					if(w > v)
						CAND.add(w);
					else
						FINI.add(w);
				}
			}
			try {
				//long t1 = System.currentTimeMillis();
				//System.out.println("K: " + K);
				//System.out.println("CAND: " + CAND);
				//System.out.println("FINI: " + FINI);
				expand(adjacency, K, CAND, FINI);
				//long elapsed = System.currentTimeMillis() - t1;
				//System.out.println(v + "\t" + elapsed + "\t" + G.AdjList.get(v).size());
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}


	private HashMap<Integer, HashSet<Integer>> createGraph(HashSet<Integer> hashSet) {
		
		HashMap<Integer, HashSet<Integer>> adj = new HashMap<>();
		
		HashSet<Integer> S = new HashSet<>();
		
		//S.addAll(hashSet);
		//adj.put(w, S);
		
		for(int u : hashSet) {
			adj.put(u, new HashSet<>());
			for(int v : hashSet) {
				if(u != v) {
					if(this.G.containsEdge(u, v)) {
						if(adj.get(u) == null) {
							HashSet<Integer> X = new HashSet<>();
							X.add(v);
							//X.add(w);
							adj.put(u, X);
						} else {
							adj.get(u).add(v);
						}
						
						if(adj.get(v) == null) {
							HashSet<Integer> X = new HashSet<>();
							X.add(u);
							//X.add(w);
							adj.put(v, X);
						} else {
							adj.get(v).add(u);
						}
					}
				}
			}
		}
		
		return adj;
	}


	public void expand(HashMap<Integer, HashSet<Integer>> adjacency, TreeSet<Integer> K, Collection<Integer> CAND, Collection<Integer> FINI)
			throws IOException {
		if (CAND.isEmpty() && FINI.isEmpty()) {
			
			count.incrementAndGet();
			return;
		}
		if(CAND.isEmpty() && !FINI.isEmpty())
			return;
		int u = find_u(adjacency, CAND, FINI);

		Collection<Integer> NghOfu = adjacency.get(u);

		Iterator<Integer> candit = CAND.iterator();


		while (candit.hasNext()) {

			int q = candit.next();
			if (!NghOfu.contains(q)) {

				K.add(q);

				Collection<Integer> NghOfq = adjacency.get(q);

				HashSet<Integer> CANDq = SetOperations.intersect(CAND, NghOfq);
				

				HashSet<Integer> FINIq = SetOperations.intersect(FINI, NghOfq);
				

				expand(adjacency, K, CANDq, FINIq);

				candit.remove(); // CAND - {q}
				
				K.remove(q);

				FINI.add(q); // FINI union {q}
			}
		}
	}



	public Collection<Integer> Ngh(Graph g, int u) {

		return g.AdjList.get(u);
	}

	public int find_u(HashMap<Integer, HashSet<Integer>> adjacency, Collection<Integer> CAND, Collection<Integer> FINI /*
															 * Set<String> SUBG
															 */) {


		int size = -1;
		int v = 0;
		for(int u : CAND){
			HashSet<Integer> Q = SetOperations.intersect(adjacency.get(u), CAND);
			if(size < Q.size()){
				size = Q.size();
				v = u;
			}
		}

		for(int u : FINI){
			HashSet<Integer> Q = SetOperations.intersect(adjacency.get(u), CAND);
			if(size < Q.size()){
				size = Q.size();
				v = u;
			}
		}
		return v; 
		

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", args[1]);
		
		Graph G = new Graph(args[0]);
		
		System.out.println("PECODegree: degree based ordering with subgraph creation for each subproblem");
		System.out.println("Input Graph: " + args[0]);
		
		System.out.println("Graph Reading Complete");
		//Graph G = new Graph("reduced_web_google_001_adj", 1);

		try {
			long t1 = System.currentTimeMillis();
			new PECODegree(G, "output_TTT");
			long elapsed = System.currentTimeMillis() - t1;
			System.out.println("number of maximal cliques: " + count.get());
			System.out.println("Parallel time taken to compute maximal cliques in" +  args[0] + ": " + elapsed/1000. + " sec.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// new Tomita(G);
	}

}
