package Algorithm.parMCE;

import Algorithm.Graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicLong;

public class Peamc {
    static AtomicLong nMaximalCliques;
    Graph G ;
    Peamc () {
        nMaximalCliques = new AtomicLong(0) ;
    }

    boolean isMaximal (ArrayList <Integer> clq) {
        HashSet <Integer> clqSet = new HashSet<>(clq) ;
        for (Integer vertex : clq) {
            for (Integer neighbor : this.G.AdjList.get(vertex)) {
                if (clqSet.contains(neighbor))
                    continue;
                boolean isConnectedToAll = true;
                for (Integer vertexInClq : clq) {
                    if (this.G.AdjList.get(neighbor).contains(vertexInClq) == false) {
                        isConnectedToAll = false;
                    }
                }
                if (isConnectedToAll == true)
                    return false;
            }
        }
        return true ;
    }

    void print (ArrayList <Integer> clq) {
        System.out.printf("%d: ", clq.size());
        for (Integer vertex : clq) {
            System.out.print(vertex + ",");
        }
        System.out.println();
    }

    public void clique (ArrayList <Integer> clq, HashSet<Integer> T, int vx) {
        for (Integer vj : T) {
            int vk = G.numV() ;
            HashSet <Integer> newT = new HashSet<>() ;
            for (Integer neighbor : this.G.M.get(vj)) {
                if (T.contains(neighbor) == true) {
                    newT.add(neighbor) ;
                    vk = Math.min(vk, neighbor) ;
                }
            }
            clq.add(vj) ;
            if (newT.isEmpty() == false) {
                clique(clq, newT, vk);
            }else {

                if (isMaximal (clq)) {
                    //print (clq) ;
                    //nMaximalCliques++;
                	nMaximalCliques.incrementAndGet();
                }
            }
            clq.remove(clq.size() - 1) ;
        }
    }

    public static void main(String[] args) {
        System.out.println("Parallel Peamc Algorithm");
        System.out.println("Input Graph: " + args[0]);
        System.out.println("Number of threads used: " + args[1]);
        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", args[1]);
        Peamc instance = new Peamc() ;
        instance.G = new Graph(args[0]);
        System.out.println("Graph Reading Complete");
        System.out.println("Number of Vertices: " + instance.G.numV());
        System.out.println("Number of Edges: " + instance.G.numE());

        instance.G.buildM();

        long begTime = System.currentTimeMillis();
        //for (Integer vertex : instance.G.vertices) {
        //    ArrayList <Integer> clq = new ArrayList<>(Arrays.asList(vertex));
        //    instance.clique(clq, instance.G.M.get(vertex), vertex);
        //}
        instance.G.vertices.parallelStream().forEach(vertex -> {
            ArrayList <Integer> clq = new ArrayList<>(Arrays.asList(vertex));
            instance.clique(clq, instance.G.M.get(vertex), vertex);
        });
        long elapsed = System.currentTimeMillis() - begTime;
        System.out.println("number of maximal cliques: " + nMaximalCliques.get());
        System.out.println("Sequential time taken to compute maximal cliques in" + args[0] + ": " + elapsed/1000. + " sec.");
    }
}
