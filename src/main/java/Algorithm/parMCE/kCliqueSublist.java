package Algorithm.parMCE;

import java.util.*;

public class kCliqueSublist {
	
	
	private TreeSet<Integer> C;
	private ArrayList<Integer> A;	//for keeping vertices in increasing order of the indices
	
	private BitSet B;
	
	private int id;
	
	public kCliqueSublist(int v, Collection<Integer> neighbors, int N, Map<Integer, BitSet> vToNghMap) {
	
		C = new TreeSet<>();
		C.add(v);
		
		A = new ArrayList<>();
		
		B = new BitSet(N);
		
		id = 0;
		for(int u : neighbors) {
			if(v <  u)
				A.add(u);
			B.set(u);
		}
		
		Collections.sort(A);
	}

	public kCliqueSublist(TreeSet<Integer> c1, ArrayList<Integer> newList, BitSet cnSkPlusOne) {
	
		C = new TreeSet<>();
		C.addAll(c1);
		
		A = new ArrayList<>();
		A.addAll(newList);
		
		B = (BitSet)cnSkPlusOne.clone();
		
	}

	public int getListSize() {
		return id;
	}

	public boolean isMaxClique(Map<Integer, BitSet> vToNghMap) {
		
		assert(C.size() == 1);
		assert(id == 1);
		
		int v = C.iterator().next();
		int w = -1;
		for(int a : A) {
			w = a;
		}
		
		BitSet b = (BitSet)vToNghMap.get(v).clone();
		
		assert(w != -1);
		
		b.and(vToNghMap.get(w));
		
		if(b.isEmpty())
			return true;
		
		return false;
			
	}

	public TreeSet<Integer> getClique() {
		
		return C;
	}

	public ArrayList<Integer> getVList() {
		
		return A;
	}

	public BitSet getCommonNeighbors() {

		return B;
	}

}
