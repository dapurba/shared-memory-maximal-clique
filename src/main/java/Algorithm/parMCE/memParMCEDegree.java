package Algorithm.parMCE;
/**This Algorithm generates all maximal cliques of an undirected graph G
 * based on Tomita et al. with title "The worst-case time complexity for generating all maximal cliques and computational experiments"
 * */

import java.util.*;

import Algorithm.Graph;
import utils.MurmurHash3;
import utils.SetOperations;

import java.io.*;
import java.util.concurrent.atomic.*;
import java.util.stream.IntStream;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ForkJoinPool;

public class memParMCEDegree {
    private static Collection<BitSet> CLQ;
    private static ConcurrentMap<Long, Integer> timeToFrequency;
    public int clqcnt;
    private static Graph G;
    static AtomicLong count;
    private static HashMap<Integer, HashSet<Integer>> MapVE;
    memoryTracker mem ;

    public memParMCEDegree(Graph g) throws IOException {
        this.mem = new memoryTracker(0, 0) ;
        this.G = g;

        count = new AtomicLong(0);

        G.vertices.parallelStream().forEach(v -> {
            mem.getReallyUsedMemory();
            Set<Integer> CAND = new HashSet<Integer>();
            Set<Integer> FINI = new HashSet<Integer>();
            TreeSet<Integer> K = new TreeSet<Integer>();

            K.add(v);
            for (int w : G.AdjList.get(v)) {
                if (G.degreeOf(w) > G.degreeOf(v))
                    CAND.add(w);
                else if (G.degreeOf(w) < G.degreeOf(v))
                    FINI.add(w);
                else {
                    if (w > v)
                        CAND.add(w);
                    else
                        FINI.add(w);
                }
            }
            try {
                //long t1 = System.currentTimeMillis();
                expand(K, CAND, FINI);
                //long elapsed = System.currentTimeMillis() - t1;
                //System.out.println(v + "\t" + elapsed + "\t" + G.AdjList.get(v).size());

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        });
        mem.getReallyUsedMemory();
    }

    public void expand(TreeSet<Integer> K, Collection<Integer> CAND, Collection<Integer> FINI) throws IOException {
        mem.getReallyUsedMemory();
        if (CAND.isEmpty() && FINI.isEmpty()) {

            count.incrementAndGet();

            return;
        }
        if(CAND.isEmpty() && !FINI.isEmpty())
            return;
        int u = find_u(CAND, FINI);
        mem.getReallyUsedMemory();
        Collection<Integer> NghOfu = Ngh(G, u);
        //List<Integer> Ext = new ArrayList<>();



        int[] Ext = new int[CAND.size()];


        int index = 0;
        for(int w : CAND){
            if (!NghOfu.contains(w)) {
                Ext[index++] = w;
            }
        }

        int size = index;

        mem.getReallyUsedMemory();
        if (size > 0) {
            IntStream.range(0, size).parallel().forEach(i -> {
                mem.getReallyUsedMemory();
                int q = Ext[i];
                TreeSet<Integer> Kq = new TreeSet<>();
                Kq.addAll(K);
                Kq.add(q);

                Collection<Integer> NghOfq = Ngh(G, q);

                HashSet<Integer> CANDq = SetOperations.intersect(CAND, NghOfq);


                HashSet<Integer> FINIq = SetOperations.intersect(FINI, NghOfq);

                for (int j = 0; j < i; j++) {
                    int p = Ext[j];
                    CANDq.remove(p);
                    if (NghOfq.contains(p))
                        FINIq.add(p);
                }

                try {
                    expand(Kq, CANDq, FINIq);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            });
        }
        mem.getReallyUsedMemory();
    }


    public Collection<Integer> Ngh(Graph G, int u) {
        return G.AdjList.get(u);
    }

    public int find_u(Collection<Integer> CAND, Collection<Integer> FINI /*
     * Set<String> SUBG
     */) {

        Map<Integer, Integer> vToIntersectSize = new ConcurrentHashMap<>();

        CAND.parallelStream().forEach(u -> {

            HashSet<Integer> Q = SetOperations.intersect(Ngh(G,u), CAND);

            vToIntersectSize.put(u, Q.size());

        });

        FINI.parallelStream().forEach(u -> {

            HashSet<Integer> Q = SetOperations.intersect(Ngh(G,u), CAND);

            vToIntersectSize.put(u, Q.size());

        });

        int size = -1;
        int v = 0;
        for(int u : vToIntersectSize.keySet()) {
            int tmp = vToIntersectSize.get(u);
            if(size < tmp) {
                size = tmp;
                v = u;
            }
        }

        return v;

    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", args[1]);

        System.out.println("ParMCEDegree : degree based vertex ordering");
        System.out.println("Input Graph: " + args[0]);
        System.out.println("Number of threads used: " + args[1]);

        Graph G = new Graph(args[0]);

        System.out.println("Graph Reading Complete");


        try {
            long t1 = System.currentTimeMillis();
            memParMCEDegree instance = new memParMCEDegree(G);
            long elapsed = System.currentTimeMillis() - t1;

            System.out.println("number of maximal cliques: " + count.get());
            System.out.println(
                    "Parallel time taken to compute maximal cliques in " + args[0] + " : " + elapsed / 1000. + " sec.");
            System.out.println("Max heap memory " + instance.mem.heap / 1024 + "MB");
            System.out.println("Max non-heap memory " + instance.mem.nonHeap / 1024 + "MB");

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // new Tomita(G);
    }

}
