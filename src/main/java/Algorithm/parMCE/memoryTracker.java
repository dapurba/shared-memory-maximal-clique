package Algorithm.parMCE;

import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;

public class memoryTracker {

    long nonHeap, heap ;

    memoryTracker (long nonHeap, long heap) {
        this.nonHeap = nonHeap ;
        this.heap = heap ;
    }

    void getCurrentlyUsedHeapMemory() {
        this.heap = Math.max (this.heap, ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed()) ;
    }

    void getCurrentlyUsedNonHeapMemory () {
        this.nonHeap = Math.max(this.nonHeap, ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage().getUsed());
    }

    long getGcCount() {
        long sum = 0;
        for (GarbageCollectorMXBean b : ManagementFactory.getGarbageCollectorMXBeans()) {
            long count = b.getCollectionCount();
            if (count != -1) { sum +=  count; }
        }
        return sum;
    }
    void getReallyUsedMemory() {
        long before = getGcCount();
        System.gc();
        while (getGcCount() == before);
        getCurrentlyUsedHeapMemory();
        getCurrentlyUsedNonHeapMemory();
    }
}
