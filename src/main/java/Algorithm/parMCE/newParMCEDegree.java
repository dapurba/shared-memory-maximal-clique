package Algorithm.parMCE;
import java.util.*;
import Algorithm.Graph;
import utils.SetOperations;
import java.io.*;
import java.util.concurrent.atomic.*;
import java.util.stream.IntStream;
import java.util.concurrent.ConcurrentHashMap;

public class newParMCEDegree {
    private static Graph G;
    static AtomicLong count;
    int depth ;

    public newParMCEDegree(Graph g, String argDepth) {
        this.G = g;
        this.depth = Integer.parseInt(argDepth) ;
        count = new AtomicLong(0);
        G.vertices.parallelStream().forEach(v -> {
            Set<Integer> CAND = new HashSet<Integer>();
            Set<Integer> FINI = new HashSet<Integer>();
            TreeSet<Integer> K = new TreeSet<Integer>();
            K.add(v);
            for (int w : G.AdjList.get(v)) {
                if (G.degreeOf(w) > G.degreeOf(v))
                    CAND.add(w);
                else if (G.degreeOf(w) < G.degreeOf(v))
                    FINI.add(w);
                else {
                    if (w > v)
                        CAND.add(w);
                    else
                        FINI.add(w);
                }
            }
            try {
                expand(K, CAND, FINI);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }

    public void seqExpand(TreeSet<Integer> K, Set<Integer> CAND, Set<Integer> FINI)
            throws IOException {
        if (CAND.isEmpty() && FINI.isEmpty()) {
            count.incrementAndGet();
            return;
        }
        if(CAND.isEmpty() && !FINI.isEmpty())
            return;
        int u = seqFind_u(CAND, FINI);
        Collection<Integer> NghOfu = this.G.AdjList.get(u) ;
        Iterator<Integer> candit = CAND.iterator();

        while (candit.hasNext()) {
            int q = candit.next();
            if (!NghOfu.contains(q)) {
                K.add(q);
                Collection<Integer> NghOfq = Ngh(G, q);
                HashSet<Integer> CANDq = SetOperations.intersect(CAND, NghOfq);
                HashSet<Integer> FINIq = SetOperations.intersect(FINI, NghOfq);
                expand(K, CANDq, FINIq);
                candit.remove(); // candidates - {q}
                K.remove(q);
                FINI.add(q); // FINI union {q}
            }
        }
    }

    public void expand(TreeSet<Integer> K, Collection<Integer> CAND, Collection<Integer> FINI) throws IOException {
        if (CAND.isEmpty() && FINI.isEmpty()) {
            count.incrementAndGet();
            return;
        }
        if(CAND.isEmpty() && !FINI.isEmpty())
            return;
        int u = find_u(CAND, FINI);
        Collection<Integer> NghOfu = Ngh(G, u);

        int[] Ext = new int[CAND.size()];


        int size = 0;
        for(int w : CAND){
            if (!NghOfu.contains(w)) {
                Ext[size++] = w;
            }
        }

        if (size > 0) {
            IntStream.range(0, size).parallel().forEach(i -> {
                int q = Ext[i];
                TreeSet<Integer> Kq = new TreeSet<>();
                Kq.addAll(K);
                Kq.add(q);
                Collection<Integer> NghOfq = Ngh(G, q);
                HashSet<Integer> CANDq = SetOperations.intersect(CAND, NghOfq);
                HashSet<Integer> FINIq = SetOperations.intersect(FINI, NghOfq);
                for (int j = 0; j < i; j++) {
                    int p = Ext[j];
                    CANDq.remove(p);
                    if (NghOfq.contains(p))
                        FINIq.add(p);
                }
                try {
                    if (Kq.size() <= depth)
                        expand(Kq, CANDq, FINIq);
                    else
                        seqExpand(Kq, CANDq, FINIq);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            });
        }
    }

    public Collection<Integer> Ngh(Graph G, int u) {
        return G.AdjList.get(u);
    }

    public int seqFind_u(Set<Integer> CAND, Set<Integer> FINI) {
        int size = -1;
        int v = 0;
        for (int u : CAND) {
            HashSet<Integer> Q = SetOperations.intersect(Ngh(G,u), CAND);
            int tmp = Q.size();
            if (size <= tmp) {
                size = tmp;
                v = u;
            }
        }
        for (int u : FINI) {
            HashSet<Integer> Q = SetOperations.intersect(Ngh(G,u), CAND);
            int tmp = Q.size();
            if (size <= tmp) {
                size = tmp;
                v = u;
            }
        }
        return v;

    }


    public int find_u(Collection<Integer> CAND, Collection<Integer> FINI) {
        Map<Integer, Integer> vToIntersectSize = new ConcurrentHashMap<>();
        FINI.parallelStream().forEach(u -> {
            HashSet<Integer> Q = SetOperations.intersect(this.G.AdjList.get(u), CAND);
            vToIntersectSize.put(u, Q.size());
        });
        CAND.parallelStream().forEach(u -> {
            HashSet<Integer> Q = SetOperations.intersect(this.G.AdjList.get(u), CAND);
            vToIntersectSize.put(u, Q.size());
        });
        int size = -1;
        int v = 0;
        for(int u : vToIntersectSize.keySet()) {
            int tmp = vToIntersectSize.get(u);
            if(size < tmp) {
                size = tmp;
                v = u;
            }
        }
        return v;
    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", args[1]);

        System.out.println("new ParMCEDegree : degree based vertex ordering");
        System.out.println("Input Graph: " + args[0]);
        System.out.println("Number of threads used: " + args[1]);

        Graph G = new Graph(args[0]);



        System.out.println("Graph Reading Complete");


        long t1 = System.currentTimeMillis();
        newParMCEDegree instance = new newParMCEDegree(G, args [2]);

        long elapsed = System.currentTimeMillis() - t1;

        System.out.println("number of maximal cliques: " + count.get());
        System.out.println("Parallel time taken to compute maximal cliques in " + args[0] + " : " + elapsed / 1000. + " sec.");
        // new Tomita(G);
    }

}
