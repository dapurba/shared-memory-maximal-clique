package Algorithm.parMCE;
/**This Algorithm generates all maximal cliques of an undirected graph G
 * based on Tomita et al. with title "The worst-case time complexity for generating all maximal cliques and computational experiments"
 * */

import java.util.*;

import Algorithm.Graph;
import utils.MurmurHash3;
import utils.SetOperations;

import java.io.*;
import java.util.concurrent.atomic.*;
import java.util.stream.IntStream;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ForkJoinPool;

public class perVertexParTTT_Total {

	private static Collection<BitSet> CLQ;

	private static ConcurrentMap<Long, Integer> timeToFrequency;
	public int clqcnt;

	private static Graph G;

	static AtomicLong count;

	private static HashMap<Integer, HashSet<Integer>> MapVE;

	public perVertexParTTT_Total(Graph g) throws IOException {

		this.G = g;

		count = new AtomicLong(0);
		
		ArrayList<Integer> vOrderDegree = G.getDegreeOrdering();

		G.vertices.forEach(v -> {
			Set<Integer> CAND = new HashSet<Integer>();
			Set<Integer> FINI = new HashSet<Integer>();
			TreeSet<Integer> K = new TreeSet<Integer>();

			K.add(v);
			for (int w : G.AdjList.get(v)) {
				if (G.degreeOf(w) > G.degreeOf(v))
					CAND.add(w);
				else if (G.degreeOf(w) < G.degreeOf(v))
					FINI.add(w);
				else {
					if (w > v)
						CAND.add(w);
					else
						FINI.add(w);
				}
			}
			try {
				long t1 = System.currentTimeMillis();
				int size_cand = CAND.size();
				int size_fini = FINI.size();
				expand(K, CAND, FINI);
				long elapsed = System.currentTimeMillis() - t1;
				System.out.println(v + "\t" + (double)elapsed/1000.0 + "\t" + size_cand + "\t" + size_fini);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}

	public void expand(TreeSet<Integer> K, Collection<Integer> CAND, Collection<Integer> FINI) throws IOException {
		if (CAND.isEmpty() && FINI.isEmpty()) {

			count.incrementAndGet();
			return;
		}
		if(CAND.isEmpty() && !FINI.isEmpty())
			return;
		int u = find_u(CAND, FINI);

		Collection<Integer> NghOfu = Ngh(G, u);

		Map<Integer, Integer> Ext = new HashMap<>();

		int[] ExtForFini = new int[CAND.size()];

		int index = 0;
		for (int w : CAND) {
			if (!NghOfu.contains(w)) {
				Ext.put(w, index);
				ExtForFini[index] = w;
				index++;
			}
		}

		int size = Ext.size();

		if (size > 0) {
			Ext.keySet().parallelStream().forEach(q -> {

				int idx = Ext.get(q);
				TreeSet<Integer> Kq = new TreeSet<>();
				Kq.addAll(K);
				Kq.add(q);

				Collection<Integer> NghOfq = Ngh(G, q);

				Collection<Integer> CANDq = computeNewCand(CAND, NghOfq, Ext, idx);
				Collection<Integer> FINIq = computeNewFini(FINI, NghOfq, Ext, ExtForFini, idx);

				// loopoverhead += (System.currentTimeMillis() - start);

				// loopoverhead.addAndGet(delta);

				try {
					expand(Kq, CANDq, FINIq);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			});
		}
	}

	private Collection<Integer> computeNewFini(Collection<Integer> fini, Collection<Integer> nghOfq,
											   Map<Integer, Integer> ext, int[] extforfini, int idx) {

		Collection<Integer> R = new HashSet<>();
		if (2 * nghOfq.size() < fini.size() + idx) {
			for (int w : nghOfq) {
				if (fini.contains(w)) {
					R.add(w);
				}
				if ((ext.get(w) < idx) && ext.containsKey(w))
					R.add(w);
			}
		}
		else {
			for (int w : fini) {
				if (nghOfq.contains(w))
					R.add(w);
			}
			for(int i=0; i < idx; i++){
				int x = extforfini[i];
				if(nghOfq.contains(x))
					R.add(x);
			}
		}
		return R;
	}

	private Collection<Integer> computeNewCand(Collection<Integer> cand, Collection<Integer> nghOfq,
											   Map<Integer, Integer> ext, int idx) {

		Collection<Integer> R = new HashSet<>();
		if (cand.size() > nghOfq.size()) {
			for (int w : nghOfq) {
				if (cand.contains(w)) {
					if (ext.containsKey(w) && (ext.get(w) > idx))
						R.add(w);
					if (!ext.containsKey(w))
						R.add(w);
				}
			}
		} else {
			for (int w : cand) {
				if (nghOfq.contains(w)) {
					if (ext.containsKey(w) && (ext.get(w) > idx))
						R.add(w);
					if (!ext.containsKey(w))
						R.add(w);
				}
			}
		}
		return R;
	}

	public Collection<Integer> Ngh(Graph G, int u) {

		return G.AdjList.get(u);
	}

	public int find_u(Collection<Integer> CAND, Collection<Integer> FINI /*
															 * Set<String> SUBG
															 */) {

		Map<Integer, Integer> vToIntersectSize = new ConcurrentHashMap<>(); 
		
		CAND.parallelStream().forEach(u -> {
			
			HashSet<Integer> Q = SetOperations.intersect(Ngh(G,u), CAND);
			
			vToIntersectSize.put(u, Q.size());
			
		});
		
		FINI.parallelStream().forEach(u -> {
			
			HashSet<Integer> Q = SetOperations.intersect(Ngh(G,u), CAND);
			
			vToIntersectSize.put(u, Q.size());
			
		});
		
		int size = -1;
		int v = 0;
		for(int u : vToIntersectSize.keySet()) {
			int tmp = vToIntersectSize.get(u);
			if(size < tmp) {
				size = tmp;
				v = u;
			}
		}

		return v;

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", args[1]);
		
		System.out.println("PerVertexParTTT : measuring ParTTT time for each vertex (with degree based vertex ordering)");
		System.out.println("Input Graph: " + args[0]);
		System.out.println("Number of threads used: " + args[1]);

		Graph G = new Graph(args[0]);

		System.out.println("Graph Reading Complete");
		
		System.out.println("Runtime of ParTTT for each subproblem");
		
		System.out.println("verex(subproblem)\tcomputation-time-of-the-subproblem(sec.)\tinitial-cand-size\tinitial-fini-size");
		

		try {
			long t1 = System.currentTimeMillis();
			new perVertexParTTT_Total(G);
			long elapsed = System.currentTimeMillis() - t1;
			
			System.out.println("number of maximal cliques: " + count.get());
			System.out.println(
					"Parallel time taken to compute maximal cliques in " + args[0] + " : " + elapsed / 1000 + " sec.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// new Tomita(G);
	}

}
