package utils;

import java.util.Comparator;
import java.util.Map;

public class ValueComparator<T> implements Comparator<T>{

	Map<T, Long> base;
	public ValueComparator(Map<T, Long> base){
		this.base = base;
	}
	
	@Override
	public int compare(T arg0, T arg1) {
		return Long.compare(base.get(arg0), base.get(arg1));
		//System.out.println(base.get(arg0));
		//if(base.get(arg0) < base.get(arg1))
		//	return -1;
		//else if(base.get(arg0) > base.get(arg1))
		//	return 1;
		//else
		//	return 0;
	}

}
